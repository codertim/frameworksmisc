

// twitter search response fields
//   created_at:
//   from_user:
//   from_user_id:
//   from_user_id_str:
//   from_user_name:
//   geo:
//   id:
//   id_str:
//   iso_language_code:
//   metadata:
//   profile_image_url:
//   profile_image_url_https:
//   source:
//   text:
//   to_user:
//   to_user_id:
//   to_user_id_str:
//   to_user_name:

// TODO: make language passed in as url query term

// requires
var http        = require("http");
var url         = require("url");
var querystring = require("querystring");


// global variables
var displayResult     = "";
var DEBUG             = false;
var defaultSearchTerm = 'node.js';
var isTerminalMode    = false;
var preferredLanguage = "en";   // each tweet as a language attribute
var searchTerm        = defaultSearchTerm;
var unescaped         = "";
var webResponse       = "";


console.log("Args: " + process.argv);
console.log("Args length: " + process.argv.length);


if(process.argv.length > 2 && process.argv[2] == 'terminal') {
	isTerminalMode = true;
	searchTerm = process.argv[2];
	console.log("Detected terminal mode - set search term to: " + searchTerm);
}


function showAttributes(myObject) {
	for(a in myObject) {
		console.log(a + ": " + myObject[a]);
	}
}



// make api call to twitter
function startTwitterSearch(mainRes) {

		// twitter info
		var twitterOptions = {
			host: 'search.twitter.com',
			path: '/search.json?q=' + searchTerm
		}

		// call twitter api
		http.get(twitterOptions, function(res) {
			var data = "";
			var json;

			res.on("data", function(data_part) {
				data += data_part;
			});

			res.on("end", function() {
				json = JSON.parse(data);
				// console.log("Twitter data= " + data);
				console.log("Result of JSON.parse = " + json);
				console.log("Result of json.results = " + json.results);
				// console.log("Result of JSON.stringify = " + JSON.stringify(json));


				if(!isTerminalMode) {
					webResponse = " ";  // reset so does not keep appending to same page
					webResponse += "<ul>";
				}

				// add relevant info for each tweet to output
				for(var i = 0; i < json.results.length; i++) {
					// console.log("\n**********");
					// showAttributes(json.results[i]);
					// console.log(json.results[i].iso_language_code);
					if(json.results[i].iso_language_code === preferredLanguage) {
						// select english tweets
						displayResult = json.results[i].text + " - " + json.results[i].from_user_name;
						if(isTerminalMode) {
							console.log(displayResult);
						} else {
							webResponse += ("<li>" + displayResult + "</li>");
						}
					}
				}

				if(!isTerminalMode) {
					webResponse += "</ul>";
				}


				// finish output processing
				if(!isTerminalMode) {
					unescaped = querystring.unescape(webResponse);
				} else {
					console.log("Process exiting with status: 0");
					process.exit(0);
				}

				mainRes.end(wrapInHtml(webResponse, "Twitter Search Results", searchTerm) + "\n");

				console.log("\n\n");
			});
		});
}



// prepare for web page display (not terminal mode)
function wrapInHtml(bodyContent, title, searchTerm) {
	var fullPage = "<!DOCTYPE html>\n<html><title>" + title + "</title><body style='background-color:#00C0F7;'>";
	fullPage += "<h2>" + title + "</h2>"
	fullPage += "<h5>Search Term: &nbsp; <mark> " + searchTerm + " </mark> </h2>"
	fullPage += "<br />";
	fullPage += bodyContent;
	fullPage += "</body></html>";
	return fullPage;
}



if(!isTerminalMode) {
	// we are not in terminal mode, so we will display in browser
	http.createServer(function(req, res) {
 		console.log("Request - req.url = " + req.url);
		var reqQuery = url.parse(req.url, true).query;   // pass true 2nd arg to parse query string
 		console.log("Request query = " + reqQuery);
 		console.log("Request query - term = " + reqQuery.term);

		if(reqQuery.term) {
			searchTerm = reqQuery.term;
		} else {
			searchTerm = defaultSearchTerm;
		}

		res.writeHead(200, {'Content-Type': 'text/html'});
		startTwitterSearch(res);
		// console.log("unescaped = " + unescaped);
		// res.write(unescaped);
	}).listen(process.env.VMC_APP_PORT || 1337, null);   // port env for appfog
}



