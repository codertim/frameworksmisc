package springmvc.web;
 
import java.io.IOException;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.mvc.SimpleFormController;

import springmvc.model.TableInfo;
 
public class TableDescriptionController extends SimpleFormController {


	/*
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String aMessage = "Hello World MVC!";
 
		ModelAndView modelAndView = new ModelAndView("hello_there");
		modelAndView.addObject("message", aMessage);
 
		return modelAndView;
	}
	*/

	public TableDescriptionController() {
		setCommandClass(TableInfo.class);
	}

/*j
	public void doSubmitAction(Object command) {
		TableInfo info = (TableInfo) command;
		process(info);
	}
*/

	@Override
	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		System.out.println("##### TableDescriptionController#onSubmit - Starting ...");
		TableInfo tableInfo = (TableInfo) command;
		System.out.println("param = " + tableInfo.getDesc());
		String sqlDdl = getCreateTableSql(tableInfo.getDesc());
		ModelAndView modelAndView = new ModelAndView("showTableDdl", "tableInfo", tableInfo);
		modelAndView.addObject("sql_output", sqlDdl);
		return modelAndView;
	}


	private String getCreateTableSql(String desc) {
		String finalSql = "";
		String sqlTable = getTableName(desc);   // "create table blahblah";

		String [] inputLines = desc.split("\n");
		System.out.println("inputLines length = " + inputLines.length);
		String allColString = constructAllColString(inputLines);
		System.out.println("allColString = " + allColString);
		finalSql = makeSqlCreateQuery(sqlTable, allColString);

		return finalSql;
	}



	// combine table name with field names
	private String makeSqlCreateQuery(String table, String cols) {
		String sql = "create table ";

		if(table != null) {
			sql += (" " + table + " " + cols);
		}

		String sqlTrimmed = sql.trim();

		String finalSql = sqlTrimmed;
		if(sqlTrimmed.endsWith(",")) {
			// chop off last character if comma
			finalSql = sqlTrimmed.substring(0, (sqlTrimmed.length() - 1));
		}

	    return finalSql;	
	}



	// combine all columns with their types into a single string
	private String constructAllColString(String [] allLines) {
		String allColString ="";

		if(allLines != null) {
			boolean isColSection = false;

			// look at the lines to get column/field info
			for(int i=0; i < allLines.length; i++) {
				String currentLine = allLines[i];
				System.out.println("     currentLine = " + currentLine);
				if(isColSection) {
					String [] fields = currentLine.split("\\|");
					if( (fields != null) && (fields.length >= 3)) {
						System.out.println("     fields[0] = " + fields[0]);
						System.out.println("     fields.length = " + fields.length);
						String currentCol = fields[0].trim();
						String currentType = fields[1].trim();
						// TODO: change to StringBuffer
						allColString += (" " + currentCol + " " + currentType);

						if(i <= allLines.length ) {
							// separate columns with comma
							allColString += ",";
						}
					}

				} else {
					if(currentLine.indexOf("Column") >= 0) {
						isColSection = true;   // the rest of the lines should be columns
					}
				}
			}
		} else {
			System.out.println("##### ERROR - constructAllColString - ERROR - line arrary is null");
		}
		
		return allColString;
	}


	// take input from web from that user copy/pasted from sql schema, e.g. from copyin "\d mytable" in postgresql
	private String getTableName(String userInput) {
		String tableName = "";

		if(userInput != null) {
			String [] lines = userInput.split("\n");
			if(lines != null) {
				for(int i=0; i < lines.length; i++) {
					System.out.println("Current line: " + lines[i]);
					int matchIndex = lines[i].indexOf("Table \"public");
					if(matchIndex > -1) {
						// we have a match: found line with table name
						System.out.println("  Start Index = " + matchIndex);
						int endIndex = lines[i].indexOf("\"", (matchIndex+14));
						System.out.println("  End Index = " + endIndex);
						int startIndex = matchIndex + 14;
						tableName = lines[i].substring(startIndex, endIndex);
					}
				}		
			}
		}

		System.out.println("##### getTableName - returning table name = |" + tableName + "|");

		return tableName;
	}
}

